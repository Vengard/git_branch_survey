let jsonfile = require('jsonfile');
let git = require("nodegit");
let path = require('path');
let configuration = require('../common/configuration.json');
let jsonPath = '../git_branch_survey/common/configuration.json';
let init = false;

module.exports = class GitUtils {
    constructor(url, pathToRepo) {
        if(GitUtils.isEmpty(configuration.url)){init=true};
        (configuration.url.localeCompare(url)===0 ? this._gitRepoUrl = configuration.url : this._gitRepoUrl = url);
        this._pathToCloneRepo = pathToRepo;
        this._repoGitName ='';
        this._gitRepoLocalPath='';
    }
    
    get repoUrl() {
        return this._gitRepoUrl;
    }
    
    get repoLocalPath() {
        return this._gitRepoLocalPath;
    }
    
    get pathToCloneRepo() {
        return this._pathToCloneRepo;
    }
    
    get repoGitName() {
        return this._repoGitName;
    }
    
    saveInJSONFile(){
        return new Promise((resolve, reject) => {
            let properties = {
                url:this._gitRepoUrl,
                gitRepoLocalPath:this._gitRepoLocalPath,
                pathToCloneRepo:this._pathToCloneRepo,
            };
            console.log('test json  ' + JSON.stringify(properties));
            jsonfile.writeFile(jsonPath, properties)
            .then((result) => {
                console.log('Write complete ');
                resolve(true);
            })
            .catch(error => {
                console.log('Write failed ' + error);
                reject(false)
            });
        });
    }
    
    checkGitRepositoryExist(){
        return new Promise((resolve, reject) => {
            let repoInstance = '';
            if(GitUtils.isEmpty(configuration.gitRepoLocalPath) && GitUtils.isEmpty(configuration.gitRepoURL)){
                console.log('json url ' + configuration.url);
                this._repoGitName = path.basename(configuration.url).substring(0,path.basename(configuration.url).lastIndexOf('.'));
                const repoLocalName = path.basename(configuration.gitRepoLocalPath).substring(0,path.basename(configuration.gitRepoLocalPath).lastIndexOf('/'));
                console.log('repogit name ' + this._repoGitName);
                console.log('gitlocal path ' +repoLocalName);
                if(this._repoGitName.localeCompare(repoLocalName)===0){
                    console.log('plop');
                    resolve(true);
                }else{    
                    resolve(false);
                }
            }else{
                resolve(false);
            }
        });
    }
    
    gitCloneRepository(){
        return new Promise((resolve, reject) => {
            let gitUrl = configuration.url,
            cloneOpts = {};
            console.log('test parameter '+ this._repoGitName);
            let localRepo = configuration.pathToCloneRepo+'/'+this._repoGitName + '/';
            console.log('new repo local ', localRepo);
            git.Clone(gitUrl, localRepo, cloneOpts)
            .then((repository) => {
                console.log("cloned " + path.basename(gitUrl) + " to " + repository.workdir() + 'local repo to set ' + localRepo);
                resolve(repository);
                this._gitRepoLocalPath = localRepo;
                this.saveInJSONFile()
                .then((resultJsonWrite) =>{
                    console.log('json update succeded');
                    resolve(repository)
                }).catch(function (err){
                    console.log('Json failed, error during update of config json => ' + err);
                });  
            }).catch(function (err) {
                console.log(err);
                reject(err);
            });
        });
    }
    
    gitCheckoutMasterBeforeToWork(){
        //TODO : return repo instance after chekcout to permit to create new branches if it's necessary
        return new Promise((resolve, reject) => {
            if(GitUtils.isEmpty(repoParam)){
                git.Repository.open(configuration.gitRepoLocalPath)
                .then(function(repo) {
                    repositoryInstance = repo;
                }).catch(function (err) {
                    console.log(err);
                });
            }
            
            console.log('current branch ' +  repositoryInstance.getCurrentBranch());
            repositoryInstance.getCurrentBranch()
            .then(function(ref) {
                console.log("On " + ref.shorthand() + " " + ref.target());
                console.log("Checking out master");
                var checkoutOpts = {
                    checkoutStrategy: nodegit.Checkout.STRATEGY.FORCE
                };
                return repo.checkoutBranch("master", checkoutOpts);
            }).then(function () {
                return repo.getCurrentBranch().then(function(ref) {
                    console.log("On " + ref.shorthand() + " " + ref.target());
                    return ref;
                });
            }).catch(function (err) {
                console.log(err);
            })
            .done(function () {
                console.log('Finished');
            });
        });
    }
    
    
    
    static isEmpty(val) {
        
        // test results
        //---------------
        // []        true, empty array
        // {}        true, empty object
        // null      true
        // undefined true
        // ""        true, empty string
        // ''        true, empty string
        // 0         false, number
        // true      false, boolean
        // false     false, boolean
        // Date      false
        // function  false
        
        if (val === undefined)
        return true;
        
        if (typeof (val) == 'function' || typeof (val) == 'number' || typeof (val) == 'boolean' || Object.prototype.toString.call(val) === '[object Date]')
        return false;
        
        if (val == null || val.length === 0)        // null or 0 length array
        return true;
        
        if (typeof (val) == "object") {
            // empty object
            
            var r = true;
            
            for (var f in val)
            r = false;
            
            return r;
        }
        
        return false;
    }
    
}