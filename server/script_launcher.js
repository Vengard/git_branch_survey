const GitUtils = require('../server/GitUtils.js');

(async function() {
    let gitInstance = new GitUtils('https://github.com/Krau/fiddle-engine.git','C:/Workspace/Perso');
    gitInstance.saveInJSONFile()
    .then((saveJsonResult)=>{
        console.log('test json result ' + saveJsonResult);
        if(saveJsonResult){
            gitInstance.checkGitRepositoryExist().then(repoExist =>{
                if(repoExist){
                    gitInstance.gitCheckoutMasterBeforeToWork();
                }else{
                    gitInstance.gitCloneRepository();
                }
            });
        }else{
            console.log('Some informations is missing in constructor parameters to check if local repository exist');  
        } 
    }).catch(error => {
        console.log('erreor ' + error);
    });
})();